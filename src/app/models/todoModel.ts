export class TodoModel {
	description:string;
	endDate:Object;
	title:string;
	userId:number;
	done:boolean;
}