import {Component, ViewChild, OnInit} from '@angular/core';
import {AuthServices} from '../services/auth.services';
import {Router} from '@angular/router';

@Component({
	selector: 'amc-header',
	templateUrl:'app/common/app.headerComponent.html'
})

export class HeaderComponent implements OnInit{

	@ViewChild('header') headerElement:any;

	constructor(private authservices :AuthServices, private router:Router ){}
	
	ngOnInit(){
		console.log(this.headerElement);
	}
	
	onLogOut(){
		this.authservices.logOut();
		return false;

	}
	
}