import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/app.loginComponent';
import {TodoComponent} from './todo/app.todoComponent';
import {MarketlistComponent} from './marketlist/app.marketlistComponent';
import {RegisterComponent} from './login/register/app.registerComponent';
import {ErrorComponent} from './error/app.errorComponent';
import {EditTodoComponent} from './todo/editForm/app.editFormComponent';

import {AuthGuardServices} from './services/authGuard.services';

const routes : Routes = [
	{path: '' , component: LoginComponent},
	{path: 'todo', component:TodoComponent, canActivate:[AuthGuardServices]},
	{path: 'edit/:todoId', component:EditTodoComponent, canActivate:[AuthGuardServices]},
	{path: 'marketlist', component:MarketlistComponent, canActivate:[AuthGuardServices]},
	{path: 'register', component: RegisterComponent},
	{path: '**', component:ErrorComponent}
];

@NgModule({
	imports : [RouterModule.forRoot(routes)],
	exports : [RouterModule]
})

export class AppRoutingModule{}
