import {Component, Input, Output, EventEmitter} from '@angular/core';
import {AuthServices} from '../../services/auth.services';
import {NgForm} from '@angular/forms';

@Component({
	selector: 'amc-registro',
	templateUrl : 'app/login/register/app.registerComponent.html'
})

export class RegisterComponent {

	messages:string;
	show:boolean= false;
	loading:boolean = false;
	itsOk:boolean = false;

	@Output() activateFlipBack = new EventEmitter();
	@Output() onSuccessUserCreation = new EventEmitter();

	constructor(private authservices:AuthServices){}

	/**
	 * [onRegister Register users]
	 * @param {NgForm} form [Form]
	 */
	onRegister(form:NgForm){
		let email = form.value.email;
		let pass = form.value.password;

		this.loading = true;
		this.show = false;

		this.authservices.createUser(email, pass)
			.then(
					(data) => {
						if(data){
							form.reset();
							this.loading = false;
							this.onSuccessUserCreation.emit('El usuario a sido creado corectamente. por favor ingresa tus datos para empezar.');
							this.flipBack(false);
						}
					}
				)
			.catch(
				(error) => {
					this.loading = false;
					this.show = true;
					
					if(error.code === 'auth/invalid-email'){
						this.messages = 'La direccion de correo que estas ingresando tiene un formato no válido.';
					}else if(error.code === 'auth/weak-password'){
						this.messages = 'La contraseña debe tener minimo 6 caracteres.';
					}else if(error.code === 'auth/email-already-in-use'){
						this.messages = 'La dirección de correo que estas ingresando, ya esta en uso.';
					}
				}
			)

	}

	flipBack(ev:boolean){
		this.activateFlipBack.emit(ev);
	}


}