import {Component, OnInit} from '@angular/core';
import {AuthServices} from '../services/auth.services';
import {NgForm} from '@angular/forms';



@Component({
	selector: 'amc-login',
	templateUrl:'app/login/app.loginComponent.html'
})

export class LoginComponent implements OnInit {
	messages:string;
	show:boolean= false;

	isVisible:boolean = false;
	isVisibleBack:boolean = false;
	itsOk:boolean = false;

	constructor (private authservices:AuthServices){}

	ngOnInit(){

		
	}

	onLogIn(form: NgForm){
		let email = form.value.email;
		let pass = form.value.password;
		
		this.authservices.loginUser(email, pass)
		.catch(
			(d) => { 
				this.show = true;
				this.messages = 'Algo esta mal con tu correo o contraseña, por favor verifica';
			}
		)
	}

	OnCreateUser(form:NgForm){
		let email = form.value.email;
		let pass = form.value.password;
		this.authservices.createUser(email, pass);
	}


	flip(){
		this.isVisible = true;
		//this.show=false;
		return false;
	}	

	activateFlipBack(nc:boolean){
		this.isVisible = nc;
		this.isVisibleBack = true;
		
	}

	onSuccessUserCreationMsg(msg:string){
		this.show = true;
		this.itsOk = true;
		this.messages = msg;
;	}

}
