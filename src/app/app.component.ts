import { Component, OnInit } from '@angular/core';
//import {firebase} from 'firebase';
import  * as firebase from 'firebase';

@Component({
  selector: 'my-app',
  templateUrl: 'app/home.html',
})

export class AppComponent implements OnInit {
		
		ngOnInit(){
			firebase.firebase.initializeApp({
				apiKey: "AIzaSyAIJQ6WPtDeydADlB6lfFBfzNkw-B8DzbI",
			    authDomain: "asic-430a5.firebaseapp.com",
			    databaseURL: "https://asic-430a5.firebaseio.com",
			    projectId: "asic-430a5",
			    storageBucket: "asic-430a5.appspot.com",
			    messagingSenderId: "64791704641"
			});
			
		}

 }
