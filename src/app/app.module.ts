import {NgModule}      from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';

import {AppComponent}  from './app.component';
import {FooterComponent} from './common/app.footerComponent';
import {HeaderComponent} from './common/app.headerComponent';
import {LoginComponent} from './login/app.loginComponent';
import {RegisterComponent} from './login/register/app.registerComponent';
import {RecoveryComponent} from './login/recovery/app.recoveryComponent';
import {TodoComponent} from './todo/app.todoComponent';
import {MarketlistComponent} from  './marketlist/app.marketlistComponent';
import {MarketListComponentForm} from  './marketlist/form/app.marketlistFormComponent';
import {ErrorComponent} from './error/app.errorComponent';
import {TodoFormComponent} from './todo/form/app.todoFormComponent';
import {TodosComponent} from './todo/todos/app.todosComponent';
import {EditTodoComponent} from './todo/editForm/app.editFormComponent';
import {AppRoutingModule} from './routing';

import {LoadingDirective} from './directives/loadingDirective';

import {AuthServices} from  './services/auth.services';
import {TodoServices} from './services/todo.services';
import {AuthGuardServices} from  './services/authGuard.services';

import {ObjectLoop} from './pipes/pipeLoopObjects'

import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports:      [ BrowserModule, RouterModule, AppRoutingModule, FormsModule, HttpModule, MyDatePickerModule],
  declarations: [ AppComponent, FooterComponent, HeaderComponent, LoginComponent, RegisterComponent, TodoComponent, MarketlistComponent, MarketListComponentForm, ErrorComponent, TodoFormComponent, TodosComponent, LoadingDirective, ObjectLoop, EditTodoComponent],
  bootstrap:    [ AppComponent ],
  providers: [AuthServices, TodoServices, AuthGuardServices]
})
export class AppModule { }
