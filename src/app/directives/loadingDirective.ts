import{Directive, ElementRef, Input, OnInit, Renderer2, HostBinding} from '@angular/core';

@Directive({
	selector :'[amcLoading]'
})

export class LoadingDirective implements OnInit{
	
	@Input() icon:string = 'fa fa-cog';
	@Input() animation:string = '';
	@Input() size:string = '16';
	@Input() color:string = '#000';

	@HostBinding('class') eleClass:string;
	@HostBinding('style.fontSize') eleSize:string;
	@HostBinding('style.color') eleColor:string;


	constructor(private el:ElementRef, private renderer: Renderer2){}

	ngOnInit(){
		this.eleClass = this.icon +' '+ this.animation;
		this.eleSize = this.size + 'px';
		this.eleColor = this.color;
	}
}