import {Component, Input, Output, EventEmitter} from '@angular/core';

import {TodoModel} from '../../models/todoModel';
import {TodoServices} from '../../services/todo.services';

import {Route, Router, ActivatedRoute} from '@angular/router';

@Component({
	selector: 'amc-todos',
	templateUrl:'app/todo/todos/app.todosComponent.html'
})

export class TodosComponent {

	@Input() todo:TodoModel;
	@Input() btnTexto:string;
	@Output() onClicked = new EventEmitter();
	textBtn:string;

	
	constructor(private todoservices:TodoServices, private router:Router, private route: ActivatedRoute){}

	ngOnInit(){
		this.getBtntext();
	}

	/**
	 * [onDeleteTodo Delete from firebase the selected item]
	 * @param {any} ele [current todo card]
	 */
	onDeleteTodo(ele:any){
		this.todoservices.deleteTodo(ele);
		return false;
	}

	onUpdateDone(ele:any){
		
		const eleKey = ele.key;
		let todoDone:boolean;

		if(ele.done){
			todoDone = false;
		}else{
			todoDone = true;
		}

		this.onClicked.emit(eleKey);

		this.todoservices.updateTodoDone({done:todoDone}, eleKey);
		this.getBtntext();
		return false;
	}

	getBtntext(){
		this.todo.done ? this.textBtn = 'Hecho' : this.textBtn = 'Por hacer';
	}

	onClickEdit(k:string){
		this.router.navigate(['/edit/', k], {relativeTo:this.route});
	}
	
	

}