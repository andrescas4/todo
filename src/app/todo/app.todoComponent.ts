import {Component, OnInit} from '@angular/core';

import {TodoServices} from '../services/todo.services';
import {TodoModel} from '../models/todoModel';
import {Response} from '@angular/http';

@Component({
	selector:'amc-todo',
	templateUrl:'app/todo/app.todoComponent.html'
})

export class TodoComponent implements OnInit{

	todosData:any[] = [];
	key:string;

	constructor(private todoservices:TodoServices){}

	ngOnInit(){
		//this.getTodosList();
		this.getTodos();
		this.getTodosAfterDelete(this.todosData);

	}

	/**
	 * [getTodos Check the list of todo in firebase, and retrive todos even if I added one.]
	 */
	getTodos(){
		let td = this.todoservices.getTodosList();
		this.todosData = td;
	}

	/**
	 * [getTodosAfterDelete remove todo from todo list after click borrar button]
	 * @param {any[]} data [array of todos]
	 */
	getTodosAfterDelete(data:any[]){
		this.todosData = this.todoservices.getTodoListAfterDelete(data);
	}

	/***/
	clickedTodo(key:string){
		 this.onUpdateTodo(this.todosData, key);
	}
	
	/***/
	onUpdateTodo(ele:any[], eleKey:string){
		this.todosData = this.todoservices.updateTodoListAfterUpdate(ele, eleKey);
	}
	
}