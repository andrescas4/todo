import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {IMyOptions} from 'mydatepicker';
import {IMyDateModel} from '../../models/dateModel';
import {TodoServices} from '../../services/todo.services';


@Component({
	selector:'amc-todoform',
	templateUrl: 'app/todo/form/app.todoFormComponent.html'
})

export class TodoFormComponent implements OnInit {
	date:Object;

	myDatePickerOptions: IMyOptions = {
	    todayBtnTxt: 'Today',
	    dateFormat: 'yyyy-mm-dd',
	    inline: false,
	    disableUntil: {year: 2016, month: 8, day: 10},
	    selectionTxtFontSize: '13px'
	};

  	constructor(private todoservices:TodoServices){}

	ngOnInit(){
	

	}
	/**
	 * [onAddTodo Method to store data in firebase, this must changed depending on your backend]
	 * @param {NgForm} form [Form object]
	 */
	onAddTodo(form:NgForm){

		const data:Object = {
			endDate: this.date ? this.date : {},
			title : form.value.title,
			description : form.value.description,
			done: false,
			userId : ''
		}

		this.todoservices.addTodo(form, data).then(
			(d) => {
				if(d === 'ok'){form.reset()}
		})
		.catch(
			(error) =>{
					console.log(error)
				}
			);
	}

	onDateChanged(event: IMyDateModel) {
		this.date = event.date;
	  	//console.log('onDateChanged(): ', event.date, ' - jsdate: ', new Date(event.jsdate).toLocaleDateString(), ' - formatted: ', event.formatted, ' - epoc timestamp: ', event.epoc);
	}



}