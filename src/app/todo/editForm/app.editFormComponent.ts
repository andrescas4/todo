import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TodoModel} from '../../models/todoModel';
import {NgForm} from '@angular/forms';

import {IMyOptions, IMyDate, IMyDateModel} from 'mydatepicker';
import {TodoServices} from '../../services/todo.services';

@Component({
	selector:'amc-editForm',
	templateUrl:'app/todo/editForm/app.editFormComponent.html'
})

export class EditTodoComponent implements OnInit{

	key:string = this.route.snapshot.params['todoId']; 
	title:string;
	description:string;
	date:Object;

	private selDate: IMyDate = {year: 0, month: 0, day: 0};

	myDatePickerOptions: IMyOptions = {
	    todayBtnTxt: 'Today',
	    dateFormat: 'yyyy-mm-dd',
	    inline: false,
	    disableUntil: {year: 2016, month: 8, day: 10},
	    selectionTxtFontSize: '13px'
	};

	constructor (private todoservices:TodoServices, private route:ActivatedRoute){}
	
	ngOnInit(){

		this.todoservices.editSingleTodo(this.key).then(
			(snapshot) => { 
				this.title = snapshot.val().title;
				this.description = snapshot.val().description;
				this.selDate = {year:snapshot.val().endDate.year, month:snapshot.val().endDate.month, day:snapshot.val().endDate.day}
			}
			)
		
	}

	onEditTodo(form:NgForm){
	
		const data:Object = {
				endDate: this.date ? this.date : this.selDate,
				title : form.value.title,
				description : form.value.description,
				done: false,
				userId : '',
				key:this.key
			}

		this.todoservices.editTodo(data, this.key);

			console.log(data);
	}


	onDateChanged(event: IMyDateModel) {
		this.date = event.date;
	  	//console.log('onDateChanged(): ', event.date, ' - jsdate: ', new Date(event.jsdate).toLocaleDateString(), ' - formatted: ', event.formatted, ' - epoc timestamp: ', event.epoc);
	}
}