import {Injectable} from '@angular/core';

@Injectable()

export class TodoServices {

	/**
	 * [getTodosList Check the list of todo in firebase, and retrive todos even if I added one.]
	 */
	getTodosList(){
		let todos:any[] = [] 
		firebase.database().ref('/todo').limitToFirst(10).on('child_added', (snapshot) => {
			todos.push(snapshot.val());
		})

		return todos;
	}

	/**
	 * [getTodoListAfterDelete Check for the array and compares the two keys, the one deleted (snapshot) and the one in the todosData array]
	 */
	getTodoListAfterDelete(todos:any[]){
		firebase.database().ref('/todo').on('child_removed', (snapshot) => {
	
			todos.forEach(
				(ele, index, arr) => {
					if(ele.key === snapshot.key){
						todos.splice(index, 1);
					}
				})//end foreach
			})

		return todos;
	}


	/**
	 * [addTodo Method to store data in firebase, this must change depending on your backend]
	 * @param {NgForm} form [Form object]
	 */
	addTodo(f:any, data:Object){
		return new Promise((resolve, reject) => {

			let todoData = data;
			let todoKey = firebase.database().ref('/todo').push().key;
			todoData['key'] = todoKey;

			firebase.database().ref('/todo/'+todoKey).set(todoData).then(
					(res) => {resolve('ok');}
				).catch(
					(error) => {console.log(error);reject(Error('mal'));}
				);
			
			
		})
		
	}

	/**
	 * [deleteTodo Delete todo from firebase]
	 * @param {any} ele [current todo]
	 */
	deleteTodo(ele:any){
		firebase.database().ref('/todo').child(ele.key).remove();
	}

	/**
	 * [updateTodoDone description]
	 * @param {Object} data    [description]
	 * @param {string} todoKey [description]
	 */
	updateTodoDone(data:Object, todoKey:string){
		firebase.database().ref('/todo/'+todoKey).update(data);
	}

	/**
	 * [updateTodoListAfterUpdate description]
	 * @param {any[]}  todos   [description]
	 * @param {string} todoKey [description]
	 */
	updateTodoListAfterUpdate(todos:any[], todoKey:string){

		firebase.database().ref('/todo/'+todoKey ).on('child_changed', (snapshot) =>{
			
			todos.forEach(
				(ele, index, arr) => {
					if(ele.key === todoKey){
						todos[index].done = snapshot.val();
					}
				})//end foreach
			})

		return todos;
		
	}

	/***/
	editTodo(data:Object, todoKey:string){

		firebase.database().ref('/todo/'+todoKey).set(data).then().catch();
	}

	editSingleTodo(todoKey:string){
		
		return firebase.database().ref('/todo/'+todoKey).once('value')


	}

	
}