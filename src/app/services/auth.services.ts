import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import{Subject} from 'rxjs/Rx';
//import {firebase} from 'firebase';
import * as firebase from 'firebase';
import {Router} from '@angular/router';

@Injectable()

export class AuthServices {

	token:any;
	
	constructor (private router:Router){}

	/**
	 * [createUser Create a new user in firebase]
	 * @param {string} email    [User mail]
	 * @param {string} password [User password]
	 */
	createUser(email:string, password:string){
		
		return firebase.firebase.auth().createUserWithEmailAndPassword(email, password)
			
	}

	/**
	 * [loginUser logen in a user and set the token in localStorage]
	 * @param {string} email    [User mail]
	 * @param {string} password [User password]
	 */
	loginUser(email:string, password:string){

		return firebase.firebase.auth().signInWithEmailAndPassword(email, password)
		.then((d:any) => { 
				
				this.router.navigate(['/todo']);

				firebase.firebase.auth().onAuthStateChanged((user:any) => {
					  if (user) {

					    this.token = user.getToken(true).then(
					    	(data:any) => {
					    		this.token = data;
					    		localStorage.setItem('amcToken', data);
					    	}
					    	);
					  } else {
					    console.log('noUser');
					  }
				});
				
			}
			)
	}

	/**
	 * [logOut LogOut from app]
	 */
	logOut(){
		return firebase.firebase.auth().signOut()
		.then((res:any) => {
			localStorage.removeItem('amcToken');
			this.router.navigate(['/'])
		})
	}

	/**
	 * [getTkn This method gets the current active user then with a promise gets the token and store in localStorage ]
	 */
	getTkn(){

		firebase.firebase.auth().onAuthStateChanged((user:any) => {
		  if (user) {
		    this.token = user.getToken(true).then(
		    	(data:any) => {
		    		if(data){
		    			return localStorage.setItem('amcToken', data);
		    		}
		    });
		  } else {
		    this.router.navigate(['/']);
		  }
		});

		
	 	
	}

	/**
	 * [isAuthenticated Gets the token of current active user and return a boolean]
	 * @return {Observable<boolean>} [description]
	 */
	isAuthenticated():Observable<boolean>{
		const subject = new Subject<boolean>();

		firebase.firebase.auth().onAuthStateChanged((user:any) => {
		  if (user) {
		    this.token = user.getToken(true).then(
		    	(data:any) => {
		    		if(data){subject.next(true);}
		    });
		  } else {
		    subject.next(false);
		    this.router.navigate(['/']);
		  }
		});

		return subject.asObservable();
	}
	

}