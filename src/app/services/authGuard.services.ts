import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import{Injectable} from '@angular/core';
import {AuthServices} from './auth.services';

@Injectable() 

export class AuthGuardServices implements CanActivate {

	constructor(private authServices:AuthServices){}

	canActivate(route:ActivatedRouteSnapshot, state: RouterStateSnapshot){
		return this.authServices.isAuthenticated().first();
	}
}